#Ionic

##Update node and npm first!
```shell
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
node -v
sudo npm install -g npm
npm -v
npm update
```

##Install frameworks
[http://ionicframework.com/getting-started]()
```shell
sudo npm install -g cordova ionic
sudo npm install -g ios-deploy ios-sim
```

Also need Android SDK and build tools and Xcode  
Had to get older Android command line tools zip and unpack the tools/template folder into the SDK's tools folder

##Create new app
```shell
ionic start One blank --v2
ionic platform add android
ionic platform add ios

ionic lab
```
[http://localhost:8100/ionic-lab]()
```shell
ionic run android
ionic run ios --device
```

##Install Cordova Plugins
###Geolocation
```shell
ionic plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="Getting location for Uber estimates"
npm install --save @ionic-native/geolocation
```
Import { Geolocation } from '@ionic-native/geolocation';
add provider!

###Native Geocoder
```shell
ionic plugin add cordova-plugin-nativegeocoder
npm install @ionic-native/native-geocoder --save
```
Import { NativeGeocoder } from '@ionic-native/native-geocoder';
add provider!

###REST HTTP Calls
[http://www.gajotres.net/ionic-2-making-rest-http-requests-like-a-pro/]()

Add provider!

index.html:
```html
<meta http-equiv="Content-Security-Policy" content="script-src 'self' 'unsafe-eval' 'unsafe-inline' *; object-src 'self'; style-src 'self' 'unsafe-inline'; media-src *">
```

###Simple Timer
```shell
npm install --save ng2-simple-timer
```

import { SimpleTimer } from 'ng2-simple-timer';
add provider!

###Background Mode
```shell
ionic plugin add cordova-plugin-background-mode
npm install --save @ionic-native/background-mode
```

Add provider!  
Only enable for iOS or make Android notification silent

###Goolge Maps
//Android debug SHA1 key:  
D5:91:7D:0C:FE:1F:1C:7A:30:8E:3D:30:FC:3C:29:D5:60:82:93:01  
API key: AIzaSyACn6XsAjPZTmUWMAym9Tyi7_ujANuxuKk

```shell
npm install --save @agm/core
```

###Keyboard
Should be already installed!

```shell
ionic plugin add ionic-plugin-keyboard
npm install --save @ionic-native/keyboard
```

Add as provider directly to component!

```ts
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
  providers: [Keyboard]
})
```

###SQLLite Storage
```shell
cordova plugin add cordova-sqlite-storage --save
```

Already installed for ionic 2!

```shell
npm install --save @ionic/storage
```

[imports]   
IonicStorageModule.forRoot()

###NG2 Charts
```shell
npm install --save ng2-charts
npm install --save chart.js
```

Import {ChartsModule} from 'ng2-charts/charts/charts';  
Import '../../node_modules/chart.js/dist/Chart.bundle.min.js';

[imports]   
ChartsModule

###Notifications
//$ ionic plugin add de.appplant.cordova.plugin.local-notification  
// fix for iOS 10?
```shell
cordova plugin add https://github.com/Artfloriani/LocalNotification
npm install --save @ionic-native/local-notifications
```

Add provider!

###In App Browser
```shell
$ ionic plugin add cordova-plugin-inappbrowser
$ npm install --save @ionic-native/in-app-browser
```

Add provider!

###Plugin Availability
```shell
ionic plugin add cordova-plugin-appavailability
npm install --save @ionic-native/app-availability
```

###Geo Calculations
//$ npm install --save geolib  
//import { geolib } from 'geolib';  
//Add provider!