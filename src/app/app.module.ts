import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
//import { AboutPage } from '../pages/about/about';
//import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
//import { TabsPage } from '../pages/tabs/tabs';
import { DetailPage } from '../pages/detail/detail';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { SimpleTimer } from 'ng2-simple-timer';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { BackgroundMode } from '@ionic-native/background-mode';
import { AgmCoreModule } from '@agm/core';
import { IonicStorageModule } from '@ionic/storage';
import { ChartsModule } from 'ng2-charts/charts/charts';
import '../../node_modules/chart.js/dist/Chart.bundle.min.js';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';

@NgModule({
  declarations: [
    MyApp,
    //AboutPage,
    //ContactPage,
    HomePage,
    //TabsPage,
    DetailPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {tabsHideOnSubPages: true}),
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyACn6XsAjPZTmUWMAym9Tyi7_ujANuxuKk'}),
    IonicStorageModule.forRoot(),
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    //AboutPage,
    //ContactPage,
    HomePage,
    //TabsPage,
    DetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    SimpleTimer,
    NativeGeocoder,
    BackgroundMode,
    LocalNotifications,
    InAppBrowser,
    AppAvailability
  ]
})
export class AppModule {}
