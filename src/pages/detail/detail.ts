import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { LoadingController } from 'ionic-angular';
import { SimpleTimer } from 'ng2-simple-timer';
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';

declare var google:any;
const PRICES = 6;
const DEFAULT_PRODUCT = "uberX";
// radius is well under 100 miles = 160 km which is the limit for Uber rides
const AUTOCOMPLETE_RADIUS = 80000;
const UBER_PRODUCTS = ["POOL", "uberX", "uberXL", "SELECT", "BLACK", "SUV", "ESPAÑOL", "ASSIST", "WAV", "LUX"]
const CORE_PRODUCTS = 3

/*
  Generated class for the Detail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html'
})
export class DetailPage {
  destination: any;
  location: string;
  address: string;
  uberServerToken: string = "t7jx5gDd1CwIdwcJQnxUW-QvvmQgu3l35JWAKJAX";
  uberClientID: string = "LRRaUXFjd9cYzJzSWGi49V08LpotdMoD";
  products: any[];
  loadingController: any;
  callback: any;
  simpleTimer: SimpleTimer;
  previousHigh: number;
  prices: any[];
  timerID: string;
  showError: any;
  map: any;
  placesQuery: any;
  places: any[];
  hasCandidate: boolean;
  index: number;
  geocoderAPI: any;
  googleAPIkey: string = "AIzaSyACn6XsAjPZTmUWMAym9Tyi7_ujANuxuKk";
  alarmState: string;
  minMaxPrice: number;
  alarmButtonPressed: boolean;
  currentProduct: string;
  alarmText: string;
  coreProducts: any[];
  listType: string;
  showSettings: boolean;
  targetPriceMode: string;
  customTarget: number;

  chartOptions: any = {
    responsive: true,
    bezierCurve: false,
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Time in min'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Price in $'
        }
      }]
    }
  };
 
  chartLabels: string[] = ['0', '2', '4', '6', '8', '10'];
  chartType: string = 'line';
  chartLegend: boolean = false;
 
  chartData: any[] = [
    { data: [0, 0, 0, 0, 0, 0], label: 'Min Price in $', lineTension: 0 },
    { data: [0, 0, 0, 0, 0, 0], label: 'Max Price in $', lineTension: 0 }
  ];

  lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http, 
    public loadingCtrl: LoadingController, 
    public zone: NgZone,
    public geocoder: NativeGeocoder, 
    public storage: Storage,
    public localNotifications: LocalNotifications, 
    public platform: Platform,
    public browser: InAppBrowser, 
    public appAvailability: AppAvailability
  ) {
    this.destination = navParams.get('destination');
    this.index = navParams.get('index');
    this.location = navParams.get('location');
    this.address = navParams.get('address');
    this.callback = navParams.get('callback');
    this.simpleTimer = navParams.get('timer');
    this.showError = navParams.get('showError');
    this.loadMap(+this.destination.latitude, +this.destination.longitude);
    this.places = [];
    this.hasCandidate = false;
    this.alarmButtonPressed = false;
    this.alarmText = "Set Alarm";
    if (this.destination.alarmState) {
      this.alarmState = this.destination.alarmState;
      if (this.alarmState == "timer") {
        this.alarmButtonPressed = true;
        this.alarmText = "Clear Alarm";
      }
    } else {
      this.alarmState = "";
    }
    if (this.alarmState === "timer" && this.destination.minMaxPrice) {
      this.minMaxPrice = this.destination.minMaxPrice;
    }
    if (this.destination.product && UBER_PRODUCTS.findIndex(product => product === this.destination.product) >= 0) {
      this.currentProduct = this.destination.product;
    } else {
      this.currentProduct = DEFAULT_PRODUCT;
      this.destination.product = this.currentProduct;
    }
    this.listType = "core";
    this.prices = [];
    this.showSettings = false;
    this.targetPriceMode = "history";
    this.customTarget = 0;
  }

  ionViewDidLoad() {
    if (this.simpleTimer.getSubscription().indexOf(this.destination.timerID) < 0) {
      this.prices = [];
      this.previousHigh = 0;
    } else {
      this.prices = this.destination.prices;
      this.previousHigh = this.prices[this.prices.length - 1].high;
      this.simpleTimer.unsubscribe(this.destination.timerID);
    }
    this.timerID = this.simpleTimer.subscribe('2min', e => this.loadHTTP());
    this.presentLoading();
    this.placesQuery = '';
  }

  ionViewCanLeave() {
    // close keyboard on iOS when leaving page
    this.setSearch('');
  }

  presentLoading() {
    this.loadingController = this.loadingCtrl.create({
      content: 'Loading from Uber...',
    });
    this.loadingController.present();
  }

  productSelected(product) {
    this.currentProduct = product.name;
    this.resetPrices();
    this.destination.product = this.currentProduct;
    this.storeDestination();
  }

  requestUber() {
    let app;

    // for ios, need to add this to info.plist in Xcode!
    // <key>LSApplicationQueriesSchemes</key>
    // <array>
    //     <string>uber</string>
    // </array>

    if (this.platform.is('ios')) {
      app = 'uber://';
    } else if (this.platform.is('android')) {
      app = 'com.ubercab';
    }

    this.platform.ready().then(() => {
      this.appAvailability.check(app).then(
        () => { // yes
          this.browser.create('uber://?action=setPickup&pickup[latitude]=' + String(this.location).split(' / ')[0] + 
            '&pickup[longitude]=' + String(this.location).split(' / ')[1] + '&pickup[nickname]=' + encodeURI(this.address) +
            '&dropoff[latitude]=' + this.destination.latitude + '&dropoff[longitude]=' + this.destination.longitude + 
            '&dropoff[nickname]=' + encodeURI(this.destination.text) + 
            '&product_id=' + this.products[this.getCurrentProductIndex()].id +
            '&client_id=' + this.uberClientID, '_system');
        },
        () => { // no   
          this.browser.create('https://m.uber.com/sign-up?client_id=' + this.uberClientID, '_system');
        }
      );
    });
  }

  settingsChanged($event) {
    if ($event === "custom") {
      this.customTarget = this.previousHigh - 1;
    }
  }

  switchAlarmState() {
    if (this.alarmState === "") {
      this.alarmState = "timer";
      this.alarmButtonPressed = true;
      this.alarmText = "Clear Alarm";
      this.showSettings = false;
      this.destination.alarmState = "timer";
      this.destination.minMaxPrice = this.minMaxPrice;
      // ensure we got permission before showing first notification
      this.localNotifications.registerPermission();
    } else {
      this.alarmState = "";
      this.alarmText = "Set Alarm";
      this.alarmButtonPressed = false;
      this.destination.alarmState = "";
      this.minMaxPrice = Math.min(...this.chartData[1]['data'].filter(function(price) {return price > 0;})) - 1;
    }
  }

  loadMap(latitude, longitude) {
    this.map = {
      lat: latitude,
      lng: longitude,
      zoom: 15
    };
  }

  autocomplete = () => {
    if (this.placesQuery == '') {
      this.places = [];
      this.hasCandidate = false;
      return;
    }
    let baseURL = '';
    if(window.location.hostname === "localhost") {
      baseURL = 'http://localhost:8100/google';
    } else {
      baseURL = 'https://maps.googleapis.com';
    }
    let url = baseURL + '/maps/api/place/autocomplete/json?input=' +  this.placesQuery +
      '&location=' + String(this.location).split(' / ')[0] + ',' + String(this.location).split(' / ')[1] +
      '&radius=' + AUTOCOMPLETE_RADIUS + '&strictbounds&key=' + this.googleAPIkey;
    this.http.get(url).map(res => res.json()).subscribe(
      data => {
        let predictions = data['predictions'];
        this.places = []; 
        let me = this;
        if (predictions.length > 0) {
          this.zone.run(function () {
            predictions.forEach(function (prediction) {
              me.places.push({
                text: prediction['description'],
                id:   prediction['place_id']
              })
            });
          });
        }
      },
      error => {
      this.showError(
        'Internet not found',
        'Please ensure that you have a working Internet connection. <br/><br/>Then come back here and press OK.',
         this.autocomplete
      );
    });
  }

  selectPlace(place: any) {
    this.hasCandidate = true;
    this.setSearch(place.text);
    // iOS local geocoder is not working great with Google suggestions
    if (this.platform.is('ios')) {
      this.getPositionGoogle(place.id);
    } else {
      this.getPositionLocal(place.text, place.id);
    }
    this.places = [];
  }

  getPositionGoogle = (placeID) => {
    let baseURL = '';
    if(window.location.hostname === "localhost") {
      baseURL = 'http://localhost:8100/google';
    } else {
      baseURL = 'https://maps.googleapis.com';
    }
    let url = baseURL + '/maps/api/geocode/json?place_id=' +  placeID +
      '&key=' + this.googleAPIkey;
    this.http.get(url).map(res => res.json()).subscribe(
      data => {
        this.loadMap(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);
      },
      error => {
      this.showError(
        'Internet not found',
        'Please ensure that you have a working Internet connection. <br/><br/>Then come back here and press OK.',
        this.getPositionGoogle, 
        placeID 
      );
    });
  }

  getPositionLocal = (address, placeID) => {
    this.geocoder.forwardGeocode(address).
      then((res: NativeGeocoderForwardResult) => {
      this.loadMap(+res.latitude, +res.longitude);
    }).catch((error) => {
      if (String(error).startsWith("Cannot get a location") || error === "cordova_not_available") {
        this.getPositionGoogle(placeID);
      } else {
        this.showError(
          'Internet not found',
          'Please ensure that you have a working Internet connection. <br/><br/>Then come back here and press OK.',
          this.getPositionLocal,
          address, 
          placeID  
        );
      }
    })
  }

  setSearch(text) {
    this.placesQuery = text;
    let searchbar = document.activeElement;
    if (searchbar instanceof HTMLElement) {
      searchbar.blur();
    }
  }

  resetPrices() {
    this.alarmState = "";
    this.alarmText = "Set Alarm";
    this.alarmButtonPressed = false;
    this.destination.alarmState = "";
    this.prices = [];
    this.previousHigh = 0;
    this.simpleTimer.unsubscribe(this.destination.timerID);
    this.timerID = this.simpleTimer.subscribe('2min', e => this.loadHTTP());
    this.presentLoading();
  }

  storeDestination() {
    this.storage.ready().then(() => {
      this.storage.set('destination_' + this.index , 
        JSON.stringify(this.destination, ['text', 'latitude', 'longitude', 'product']));
    });
  }

  setDestination() {
    this.destination.text = this.placesQuery;
    this.destination.latitude = this.map.lat;
    this.destination.longitude = this.map.lng;
    this.resetPrices();
    this.hasCandidate = false;
    this.setSearch('');
    this.storeDestination();
  }

  cancelSelection() {
    this.hasCandidate = false;
    this.setSearch('');
    this.loadMap(+this.destination.latitude, +this.destination.longitude);
  }

  getCurrentProductIndex() {
    return this.products.findIndex(product => product.name === this.currentProduct);
  }

  switchLists() {
    if (this.listType === "core") {
      this.listType = "full";
    } else {
      this.listType = "core";
    }
  }

  loadHTTP = () => {
    this.products = [];
    this.coreProducts = [];
    this.loadUberPriceEstimate().subscribe(
      data => {
        for(let i = 0; i < UBER_PRODUCTS.length; i++) {
          let sortIndex = data['prices'].findIndex(price => price.display_name.includes(UBER_PRODUCTS[i]));
          this.products.push({
            text: UBER_PRODUCTS[i] + ": " + data['prices'][sortIndex]['estimate'],
            name: UBER_PRODUCTS[i],
            id:   data['prices'][sortIndex]['product_id']
          });
          if (i < CORE_PRODUCTS) {
            this.coreProducts.push({
            text: UBER_PRODUCTS[i] + ": " + data['prices'][sortIndex]['estimate'],
            name: UBER_PRODUCTS[i],
            id:   data['prices'][sortIndex]['product_id']
          });
          }
        }

        let currentIndex = this.getCurrentProductIndex();
        // high estimate always one $ too high for POOL!
        let highEstimate = data['prices'][currentIndex]['high_estimate'] - (this.products[currentIndex].name === "POOL"?1:0);
        let trend = '';
        if (this.previousHigh != 0) {
          if (highEstimate > this.previousHigh) {
             trend = 'trending-up';
          } else if (highEstimate < this.previousHigh) {
            trend = 'trending-down';
          } else {
            trend = 'remove'
          }
        }
        this.prices.push({
          high: highEstimate,
          low: data['prices'][currentIndex]['low_estimate'],
          text: data['prices'][currentIndex]['estimate'],
          trend: trend
        })
        if (this.prices.length > PRICES) {
          this.prices.splice(0,1);
        }
        
        this.previousHigh = highEstimate;
        this.destination.estimate = data['prices'][currentIndex]['estimate'];
        this.destination.timerID = this.timerID;
        this.destination.prices = this.prices;
        this.callback(this.index, this.destination, trend);
        
        let priceTypes = ['low', 'high'];
        let _chartData:Array<any> = new Array(this.chartData.length);
        for (let i = 0; i < this.chartData.length; i++) {
          _chartData[i] = {data: new Array(this.chartData[i].data.length), label: this.chartData[i].label};
          for (let j = 0; j < this.chartData[i].data.length; j++) {
            _chartData[i].data[j] = this.prices[j] ?
              this.prices[j][priceTypes[i]] : 0;
          }
        }
        this.chartData = _chartData;

        if (this.alarmState === "timer" && highEstimate <= this.minMaxPrice) {
          this.localNotifications.schedule({
            text: "Low price of $" + this.minMaxPrice + " hit  for '" + this.destination.text + "'.",
          });
          this.switchAlarmState();
        } else if (this.alarmState === "") {
          this.minMaxPrice = Math.min(...this.chartData[1]['data'].filter(function(price) {return price > 0;})) - 1;
        }
        this.loadingController.dismiss();
      },
      error => {
        this.loadingController.dismiss();
        if (String(error).indexOf("422 Unprocessable Entity") > 0) {
          // error from Uber API - todo
        } else {
          this.showError(
            'Internet not found',
            'Please ensure that you have a working Internet connection. <br/><br/>Then come back here and press OK.',
             this.loadHTTP
          );
        }
      }
    );
  }

  loadUberPriceEstimate() {
    let headers = new Headers();
    headers.append('Authorization', 'Token ' + this.uberServerToken);
    headers.append('Accept-Language', 'en_US');
    headers.append('Content-Type', 'application/json');
    let baseURL = '';
    if(window.location.hostname === "localhost") {
      baseURL = 'http://localhost:8100/uber';
    } else {
      baseURL = 'https://api.uber.com';
    }
    let url = baseURL + '/v1.2/estimates/price?start_latitude=' + String(this.location).split(' / ')[0] + 
      '&start_longitude=' + String(this.location).split(' / ')[1] + '&end_latitude=' + 
      this.destination.latitude + '&end_longitude=' + this.destination.longitude;
    let response = this.http.get(url, {headers: headers}).map(res => res.json());
    return response;
  }
}
