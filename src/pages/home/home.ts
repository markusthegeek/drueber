import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
// https://www.npmjs.com/package/simple-timer
import { SimpleTimer } from 'ng2-simple-timer';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Platform } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

declare var google:any;
const DESTINATIONS = 5;
const DEFAULT_PRODUCT = "uberX";
const MIN_RELOAD_DISTANCE = 402;   // in meters, this is a quarter mile

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  destinations: any[];
  location: string;
  address: string;
  loadingController: any;
  alertShown: boolean;
  geocoderAPI: any;
  googleAPIkey: string = "AIzaSyACn6XsAjPZTmUWMAym9Tyi7_ujANuxuKk";

  constructor(
    public navCtrl: NavController, 
    public geolocation: Geolocation, 
    public simpleTimer: SimpleTimer, 
    public geocoder: NativeGeocoder,
    public platform: Platform, 
    public loadingCtrl: LoadingController,
    public backgroundMode: BackgroundMode, 
    public alertCtrl: AlertController, 
    public storage: Storage,
    public http: Http
  ) {
    this.alertShown = false;
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      // set notification options for Android; silent = true seems to hide like on iOS
      this.backgroundMode.setDefaults({
        title:   'Druber is running in background',
        text:    'Looking for best prices on Uber...',
        bigText: false,
        resume:  true,
        silent:  true,
        hidden:  true,
        color:   undefined,
        icon:    'icon'
      });
      this.backgroundMode.enable();
      this.platform.resume.subscribe(() => {
        this.checkPosition();
      });
      this.startup();
    });
  }

  checkPosition() {
    let options = {
      enableHighAccuracy: true,
      timeout: 5000
    };

    this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
      if (this.getDistance(String(this.location).split(' / ')[0],
        String(this.location).split(' / ')[1], position.coords.latitude, position.coords.longitude)
          > MIN_RELOAD_DISTANCE) {
        let alert = this.alertCtrl.create({
          title: "Location has changed",
          subTitle: "Your location has changed by more than a quarter mile.<br /><br />Do you want to update My Location and reset all price watches?",
          buttons: [
            {
              text: 'No',
              role: 'cancel'
            },
            {
              text: 'Yes',
              handler: () => {
                alert.dismiss().then(() => {
                  this.reload();
                });
              }
            }
          ],
          enableBackdropDismiss: true
        });
        alert.present();
      }
    });
  }

  // http://www.movable-type.co.uk/scripts/latlong.html

  getRadians(degree) {
    return degree * Math.PI / 180;
  };

  getDistance(lat1, lon1, lat2, lon2) {
    let R = 6371e3; // meters
    let φ1 = this.getRadians(lat1);
    let φ2 = this.getRadians(lat2);
    let Δφ = this.getRadians(lat2-lat1);
    let Δλ = this.getRadians(lon2-lon1);

    let a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    let d = R * c;
    return d;  // distance in meters
  }

  // end

  startup() {
    this.simpleTimer.newTimer('2min', 120);
    this.getGeoLocation(); 
  }

  reload() {
    this.loadingController = this.loadingCtrl.create({
      content: 'Restarting App...',
    });
    this.loadingController.present().then(() => {
      location.reload()
    });
  }

  presentLoading() {
    this.loadingController = this.loadingCtrl.create({
      content: 'Getting Location...',
    });
    this.loadingController.present();
  }

  setDestinations() {
    this.destinations = [];
    for (let i = 0; i < DESTINATIONS; i++) {
      this.destinations.push({
        text: this.address,
        latitude:  String(this.location).split(' / ')[0],
        longitude: String(this.location).split(' / ')[1]
      });
    }
    this.storage.ready().then(() => {
      for (let i = 0; i < DESTINATIONS; i++) {
        this.storage.get('destination_' + i).then((val) => {
          if (val) {
            this.destinations[i] = JSON.parse(val);
          } else {
            this.destinations[i].product = DEFAULT_PRODUCT;
            this.storage.set('destination_' + i , 
              JSON.stringify(this.destinations[i], ['text', 'latitude', 'longitude', 'product']));
          }
        }).catch((error) => {
          alert(error);
        });
      }
    });
  }

  showError = (title, subtitle, okFunction: (...args: any[]) => void, ...args: any[]) => {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            okFunction(...args);
            this.alertShown = false;
          }
        }
      ],
      enableBackdropDismiss: false
    });
    if (this.alertShown == false) {
      alert.present();
      this.alertShown = true;
    }
  }

  getGeoLocation = () => {
    // on iOS add the following to the product plist
    //<key>NSLocationWhenInUseUsageDescription</key>
    //<string>Getting location for Uber estimates</string>
    // manifest via:
    // ionic plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="Getting location for Uber estimates"

    this.presentLoading();

    let options = {
      enableHighAccuracy: true,
      timeout: 5000
    };

    this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
      this.location = position.coords.latitude + " / " + position.coords.longitude;
      this.getAddress(position);
      this.loadingController.dismiss();
    }).catch((error) => {
      this.loadingController.dismiss();
      this.showError(
        'Location not found', 
        this.platform.is('ios') ? 'Please go to Settings - Privacy and enable Location Services. <br/><br/>Then come back here and press OK.' :
          'Please go to Settings - Location and enable High accuracy mode. <br/><br/>Then come back here and press OK.',
        this.getGeoLocation
      );
    });
  }

  getAddress = (position) => {
    this.geocoder.reverseGeocode(position.coords.latitude, position.coords.longitude).
      then((res: NativeGeocoderReverseResult) => {
      this.address = res.houseNumber + " " + res.street + ", " + res.city + ", " + res.postalCode;
      this.setDestinations();
    }).catch((error) => {
      if (error === "cordova_not_available") {
        let baseURL = '';
        if(window.location.hostname === "localhost") {
          baseURL = 'http://localhost:8100/google';
        } else {
          baseURL = 'https://maps.googleapis.com';
        }
        let url = baseURL + '/maps/api/geocode/json?latlng=' +  position.coords.latitude + "," + 
          position.coords.longitude + '&key=' + this.googleAPIkey;
        this.http.get(url).map(res => res.json()).subscribe(
          data => {
            this.address = data.results[0].formatted_address;
            this.setDestinations();
          },
          error => {
          this.showError(
            'Internet not found',
            'Please ensure that you have a working Internet connection. <br/><br/>Then come back here and press OK.',
            this.getAddress,
            position 
          );
        });
      } else {
        this.showError(
          'Internet not found',
          'Please ensure that you have a working Internet connection. <br/><br/>Then come back here and press OK.',
          this.getAddress,
          position  
        );
      }
    });
  }

  destinationSelected(destination) {
    this.navCtrl.push(DetailPage, {
      destination: destination,
      index: this.destinations.indexOf(destination),
      location: this.location,
      address: this.address,
      callback: this.callbackEstimate,
      timer: this.simpleTimer,
      showError: this.showError
    });
  }

  callbackEstimate = (index, destination, priceTrend) => {
     return new Promise((resolve, reject) => {
       this.destinations[index] = destination;
       // if (priceTrend === 'flat') {
       //   this.destinations[index].trend = 'remove';
       // } else if (priceTrend === 'up') {
       //   this.destinations[index].trend = 'trending-up';
       // } else if (priceTrend === 'down') {
       //   this.destinations[index].trend = 'trending-down';
       // } else {
       if (priceTrend) {
         this.destinations[index].trend = priceTrend;
       }  else {
         this.destinations[index].trend = 'pulse';
       }
       // }
       resolve();
     });
  }

}
